package com.jy.pockermonsterapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MonsterCreateRequest {
    private String name;
    private String evolution;
    private String monsterType;
    private Short hp;
    private Short atkPower;
    private Short defCap;
    private Short speed;
    private String skill;
    private String imgSrc;
}
