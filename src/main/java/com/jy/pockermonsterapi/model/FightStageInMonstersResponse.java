package com.jy.pockermonsterapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FightStageInMonstersResponse {
    private MonsterItem monster1;
    private MonsterItem monster2;
}
