package com.jy.pockermonsterapi.model;

import com.jy.pockermonsterapi.entity.Monster;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MonsterItem {
    private Long stageId;
    private Monster monsterId;
    private String name;
    private String evolution;
    private String monsterType;
    private Short hp;
    private Short atkPower;
    private Short defCap;
    private Short speed;
    private String skill;
    private String imgSrc;
}
