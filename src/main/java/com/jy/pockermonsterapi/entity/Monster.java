package com.jy.pockermonsterapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Monster {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 20)
    private String evolution;

    @Column(nullable = false, length = 20)
    private String monsterType;

    @Column(nullable = false)
    private Short hp;

    @Column(nullable = false)
    private Short atkPower;

    @Column(nullable = false)
    private Short defCap;

    @Column(nullable = false)
    private Short speed;

    @Column(nullable = false, length = 20)
    private String skill;

    @Column(nullable = false)
    private String imgSrc;
}
