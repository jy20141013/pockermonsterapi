package com.jy.pockermonsterapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class FightStage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "monsterId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Monster monster;
}
