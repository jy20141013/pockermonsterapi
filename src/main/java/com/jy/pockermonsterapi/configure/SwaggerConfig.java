package com.jy.pockermonsterapi.configure;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(title = "포켓몬스터 APP",
                description = "나만의 포켓몬들끼리 대결해보자.",
                version = "v1"))
@RequiredArgsConstructor
@Configuration
public class SwaggerConfig {

    @Bean
    public GroupedOpenApi loveLineApi() {
        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group("포켓몬스터 API v1")
                .pathsToMatch(paths)
                .build();
    }
}
