package com.jy.pockermonsterapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PockerMonsterApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(PockerMonsterApiApplication.class, args);
    }

}
