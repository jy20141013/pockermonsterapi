package com.jy.pockermonsterapi.controller;

import com.jy.pockermonsterapi.entity.Monster;
import com.jy.pockermonsterapi.model.FightStageInMonstersResponse;
import com.jy.pockermonsterapi.service.FightStageService;
import com.jy.pockermonsterapi.service.MonsterService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/fight-stage")
public class FightStageController {
    private final FightStageService fightStageService;
    private final MonsterService monsterService;

    @PostMapping("/new/monster-id/{monsterId}")
    public String setStage(@PathVariable long monsterId) throws Exception{
        fightStageService.setStage(monsterId);
        return "OK";
    }

    @GetMapping("/fight-stage/current/state")
    public FightStageInMonstersResponse getCurrentState() {
        return fightStageService.getCurrentState();
    }

    @DeleteMapping("/stage/out/stage-id/{stageId}")
    public String delStageInMonster(@PathVariable long stageId) {
        fightStageService.delStageInMonster(stageId);
        return "OK";
    }
}
