package com.jy.pockermonsterapi.controller;

import com.jy.pockermonsterapi.model.MonsterCreateRequest;
import com.jy.pockermonsterapi.model.MonsterList;
import com.jy.pockermonsterapi.service.MonsterService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/monster")
public class MonsterController {
    private final MonsterService monsterService;

    @PostMapping("/new")
    public String setMonster(@RequestBody MonsterCreateRequest request) {
        monsterService.setMonster(request);
        return "OK";
    }

    @GetMapping("/all")
    public List<MonsterList> getMonsters() {
        return monsterService.getMonsters();
    }
}
