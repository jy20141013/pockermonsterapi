package com.jy.pockermonsterapi.repository;

import com.jy.pockermonsterapi.entity.Monster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MonsterRepository extends JpaRepository<Monster, Long> {
}
