package com.jy.pockermonsterapi.repository;

import com.jy.pockermonsterapi.entity.FightStage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FightStageRepository extends JpaRepository<FightStage, Long> {
}
