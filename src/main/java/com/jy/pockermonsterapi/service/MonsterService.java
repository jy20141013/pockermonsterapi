package com.jy.pockermonsterapi.service;

import com.jy.pockermonsterapi.entity.Monster;
import com.jy.pockermonsterapi.model.MonsterCreateRequest;
import com.jy.pockermonsterapi.model.MonsterList;
import com.jy.pockermonsterapi.repository.MonsterRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MonsterService {
    private final MonsterRepository monsterRepository;

    public void setMonster(MonsterCreateRequest request) {
        Monster addData = new Monster();
        addData.setName(request.getName());
        addData.setEvolution(request.getEvolution());
        addData.setMonsterType(request.getMonsterType());
        addData.setHp(request.getHp());
        addData.setAtkPower(request.getAtkPower());
        addData.setDefCap(request.getDefCap());
        addData.setSpeed(request.getSpeed());
        addData.setSkill(request.getSkill());
        addData.setImgSrc(request.getImgSrc());

        monsterRepository.save(addData);
    }

    public List<MonsterList> getMonsters() {
        List<Monster> originList = monsterRepository.findAll();
        List<MonsterList> result = new LinkedList<>();

        for (Monster monster : originList) {
            MonsterList addData = new MonsterList();
            addData.setId(monster.getId());
            addData.setName(monster.getName());
            addData.setMonsterType(monster.getMonsterType());
            addData.setHp(monster.getHp());
            addData.setAtkPower(monster.getAtkPower());
            addData.setDefCap(monster.getDefCap());
            addData.setImgSrc(monster.getImgSrc());
            addData.setSkill(monster.getSkill());
            addData.setSpeed(monster.getSpeed());
            addData.setEvolution(monster.getEvolution());

            result.add(addData);
        }

        return result;
    }
}
