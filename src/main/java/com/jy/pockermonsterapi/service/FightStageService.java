package com.jy.pockermonsterapi.service;

import com.jy.pockermonsterapi.entity.FightStage;
import com.jy.pockermonsterapi.entity.Monster;
import com.jy.pockermonsterapi.model.FightStageInMonstersResponse;
import com.jy.pockermonsterapi.model.MonsterItem;
import com.jy.pockermonsterapi.repository.FightStageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FightStageService {
    private final FightStageRepository fightStageRepository;

    public void setStage(Monster monster) throws Exception {
        List<FightStage> checkList = fightStageRepository.findAll();
        if (checkList.size() >= 2) throw new Exception();

        FightStage fightStage = new FightStage();
        fightStage.setMonster(monster);
        fightStageRepository.save(fightStage);
    }

    public void delStageInMonster(long id) {
        fightStageRepository.deleteById(id);
    }

    public FightStageInMonstersResponse getCurrentState() {
        List<FightStage> checkList = fightStageRepository.findAll();
        FightStageInMonstersResponse response = new FightStageInMonstersResponse();

        if (checkList.size() == 2) {
            response.setMonster1(convertMonsterItem(checkList.get(0)));
            response.setMonster1(convertMonsterItem(checkList.get(1)));
        } else if (checkList.size() == 1) {
            response.setMonster1(convertMonsterItem(checkList.get(0)));
        }

        return response;
    }

    private MonsterItem convertMonsterItem(FightStage fightStage) {
        MonsterItem monsterItem = new MonsterItem();
        monsterItem.setStageId(fightStage.getId());
        monsterItem.setMonsterId(fightStage.getMonster());
        monsterItem.setEvolution(fightStage.getMonster().getEvolution());
        monsterItem.setMonsterType(fightStage.getMonster().getMonsterType());
        monsterItem.setAtkPower(fightStage.getMonster().getAtkPower());
        monsterItem.setHp(fightStage.getMonster().getHp());
        monsterItem.setDefCap(fightStage.getMonster().getDefCap());
        monsterItem.setSpeed(fightStage.getMonster().getSpeed());
        monsterItem.setImgSrc(fightStage.getMonster().getImgSrc());

        return monsterItem;
    }

}
